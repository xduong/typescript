import React from 'react';
import {InterfaceDataUser} from '../DemoProps';

interface InterfaceDataInfor {
  user: InterfaceDataUser;
}

export default function UserInfor({user}: InterfaceDataInfor) {
  return (
    <div>
      <p>UserInfor</p>
      <p>{user.name}</p>
      <p>{user.age}</p>
    </div>
  );
}
