import './App.css';
import DemoProps from './DemoProps/DemoProps';
import UserInfor from './DemoProps/UserInfor/UserInfor';

function App() {
  return (
    <div className='App'>
      <DemoProps />
    </div>
  );
}

export default App;
